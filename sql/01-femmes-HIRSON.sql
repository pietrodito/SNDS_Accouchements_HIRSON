include(sql/00_user_macros.m4)
include(/sasdata/prd/users/42a001092210899/sasuser/RStudio_projects/_SNDS_TOOLS/Macros/load-macros.m4)

define([HIRSON_CODE_DPT], '002')
define([HIRSON_CODE_COM], '381')

define([ANO_femmes_en_age_de_HIRSON], [dnl
  define([ANNEE], $1)
  
  residents_commune(HIRSON_CODE_DPT,
                    HIRSON_CODE_COM,
                    '0101[]ANNEE[]',
                    ANO_HIRSON_[]ANNEE)
  
  create_table(ANO_HIRSON_FEMMES_EN_AGE_[]ANNEE_2_DIGITS)
  (select       b.BEN_NIR_ANO
    ,           ANNEE as [ANNEE]
  from        IR_BEN_R b
  inner join ANO_HIRSON_[]ANNEE h
    on       b.BEN_NIR_ANO = h.BEN_NIR_ANO
    and      b.BEN_SEX_COD = 2
    and      ANNEE - b.BEN_NAI_ANN >= 15
    and      ANNEE - b.BEN_NAI_ANN <= 50)
    union
  (select       b.BEN_NIR_ANO
    ,           ANNEE as [ANNEE]
  from        IR_BEN_R_ARC b
  inner join ANO_HIRSON_[]ANNEE h
    on       b.BEN_NIR_ANO = h.BEN_NIR_ANO
    and      b.BEN_SEX_COD = 2
    and      ANNEE - b.BEN_NAI_ANN >= 15
    and      ANNEE - b.BEN_NAI_ANN <= 50)
/

  drop_table(ANO_HIRSON_[]ANNEE)
  
  ano_psa(BEN_HIRSON_FEMMES_EN_AGE_[]ANNEE_2_DIGITS,
          ANO,
          ANO_HIRSON_FEMMES_EN_AGE_[]ANNEE_2_DIGITS)
          
  create_table(ACCOUCHEMETS_RESIDENTS_HIRSON_[]ANNEE_2_DIGITS)
  select     b.ETA_NUM
    ,        ANNEE as [ANNEE]
  from       T_MCOaaC c
  inner join BEN_HIRSON_FEMMES_EN_AGE_[]ANNEE_2_DIGITS h
    on       c.NIR_ANO_17 = h.BEN_NIR_PSA
  inner join T_MCOaaB b
    on       j2k(b, c)
  inner join T_MCOaaD d
    on       j2k(b, d)
    and      d.ASS_DGN like 'Z37%'
/

  drop_table(BEN_HIRSON_FEMMES_EN_AGE_[]ANNEE_2_DIGITS)
])


forloop([year], 2013, 2013, [
  ANO_femmes_en_age_de_HIRSON(year)
  export(ACCOUCHEMETS_RESIDENTS_HIRSON_[]ANNEE_2_DIGITS)
  export(ANO_HIRSON_FEMMES_EN_AGE_[]ANNEE_2_DIGITS)
])


